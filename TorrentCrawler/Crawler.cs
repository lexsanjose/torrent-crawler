﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Data;

namespace TorrentCrawler
{
    class Crawler
    {
        Dictionary<int, string> Proxies = ConfigParser.Proxies.ToDictionary();
        Random rand = new Random();

        private string GetCharset(string contenttype)
        {
            if(Regex.IsMatch(contenttype, @"charset=\S+"))
                return Regex.Match(contenttype, @"charset=\S+").ToString().Split('=')[1];
            return "utf-8";
        }

        private string DecompressGZip(Stream data, string charset)
        {
            MemoryStream decompressed = new MemoryStream();
            Encoding encoding = Encoding.GetEncoding(charset);
            GZipStream gzip = new GZipStream(data, CompressionMode.Decompress);
            gzip.CopyTo(decompressed);
            return encoding.GetString(decompressed.GetBuffer(), 0, (int)decompressed.Length);
        }

        private MemoryStream DecompressGZipInStream(Stream data, string charset)
        {
            MemoryStream decompressed = new MemoryStream();
            Encoding encoding = Encoding.GetEncoding(charset);
            GZipStream gzip = new GZipStream(data, CompressionMode.Decompress);
            gzip.CopyTo(decompressed);
            return decompressed;
        }

        //Using Webclient rather than Http Web Request/Response
        //Unused Method, but stays as is for reference
        private string GetSiteRawHtml2(string url)
        {
            UriBuilder toCrawl = new UriBuilder(url);
            WebClient client = new WebClient();
            Encoding encoding = Encoding.GetEncoding("windows-1251");
            byte[] data = client.DownloadData(toCrawl.Uri);
            GZipStream gzip = new GZipStream(new MemoryStream(data), CompressionMode.Decompress);
            MemoryStream ms = new MemoryStream();
            gzip.CopyTo(ms);
            return encoding.GetString(ms.GetBuffer(), 0, (int)ms.Length);

        }

        private void BuildHeaderRequest(Uri url, HttpWebRequest req)
        {
            req.UserAgent = @"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.ContentType = "text/html; charset=UTF-8";
            req.Headers.Add("Accept-Encoding: gzip, deflate");
            req.Headers.Add("Accept-Language: en-US,en,q=0.5");
            //req.CookieContainer = SetCookie(url);
        }

        //Seems I can't change Set-Cookie part of request. Needs to have a workaround here for more anonymous crawling.
        private CookieContainer SetCookie(Uri url)
        {
            CookieContainer cont = new CookieContainer();
            cont.Add(url, new Cookie("country_code", "US"));
            return cont;
        }
     
        private WebProxy GetProxy()
        {
            try
            {
                int tmp = rand.Next(1, Proxies.Count);
                return new WebProxy(Proxies[tmp].Split(':')[0], Convert.ToInt32(Proxies[tmp].Split(':')[1]));
            }
            catch
            {
                return null;
            }
        }

        private string GetSiteRawHtml(string url)
        {            
            UriBuilder toCrawl = new UriBuilder(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(toCrawl.Uri);
            BuildHeaderRequest(toCrawl.Uri, request);

            try
            {
                request.Proxy = GetProxy();
                WebResponse response = request.GetResponse();
                string[] absUri = response.ResponseUri.AbsoluteUri.Replace("%20", " ").Split('/');

                if (!Regex.IsMatch(absUri[absUri.Length - 2], "\\d+"))
                    throw new WebException("(404) Not Found.");

                if (((HttpWebResponse)response).ContentEncoding.ToString().ToUpper() == "GZIP")
                {
                    string decompressed = DecompressGZip(response.GetResponseStream(), GetCharset(response.ContentType));
                    response.Close();
                    return decompressed;
                }
                    
                else
                {

                    Stream data = response.GetResponseStream();
                    StreamReader reader = new StreamReader(data);
                    response.Close();
                    return reader.ReadToEnd();
                }
            }

            catch(WebException e)
            {
                return e.Message;
            }
        }

        private string GetNumberOfFiles(string innerhtml)
        {
            string constpattern = "<td class=\"center\">";
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        private string GetFileSize(string innerhtml)
        {
            string constpattern = "<td class=\"nobr center\">";
            string size = Regex.Match(innerhtml, constpattern + "\\S+").ToString().Substring(constpattern.Length);

            constpattern = constpattern + size + " <span>";
            size += Regex.Match(innerhtml, constpattern + "\\w+").ToString().Substring(constpattern.Length);
            return size;
        }

        private string GetMagnetLink(string innerhtml)
        {
            return Regex.Match(innerhtml, "magnet:\\S+").ToString().Replace("\"", "");
        }

        private string GetTorrentLink(string innerhtml)
        {
            return Regex.Match(innerhtml, "torcache\\.net\\S+").ToString().Replace("\"", "");
        }

        private string GetTotalSeeders(string innerhtml)
        {
            string constpattern = "<td class=\"green center\">";
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        private string GetTotalLeechers(string innerhtml)
        {
            string constpattern = "<td class=\"red lasttd center\">";
            string a = Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        private string GetTitle(string innertext)
        {
            return innertext.Split('\n')[11].Trim();
        }

        private void DownloadTorrent(string url, string filename)
        {
            UriBuilder toDownload = new UriBuilder(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(toDownload.Uri);
            BuildHeaderRequest(toDownload.Uri, request);

            WebResponse response = request.GetResponse();
            string[] absUri = response.ResponseUri.AbsoluteUri.Replace("%20", " ").Split('/');
            byte[] streamInByte = DecompressGZipInStream(response.GetResponseStream(), GetCharset(response.ContentType)).ToArray();
            response.Close();
            
            using(BinaryWriter writer = new BinaryWriter(new FileStream(filename, FileMode.Create)))
            {
                writer.Write(streamInByte);
            }
        }

        static void Main(string[] args)
        {
            Crawler prog = new Crawler();
            DataTable dt = new DataTable();
            bool hasResult = true;
            string url = @"https://www.kat.cr/usearch/{0}/{1}/";

            dt.Columns.Add("Index");
            dt.Columns.Add("Title");
            dt.Columns.Add("Resolution");
            dt.Columns.Add("Files");
            dt.Columns.Add("Size");
            dt.Columns.Add("Magnet");
            dt.Columns.Add("Torrent");
            dt.Columns.Add("Seeders");
            dt.Columns.Add("Leechers");

            if (File.Exists("report.txt"))
                File.Delete("report.txt");

            int indexer = 0, page = 0;
            string innerhtml;

            Console.Write("Enter KEY to search:\n    .>> ");
            string key = Console.ReadLine();

            Console.Write("\nEnter MAX number of pages to crawl:\n    .>> ");
            string maxPage = Console.ReadLine();

            Console.WriteLine();
            while (hasResult)
            {
                page++;
                
                if (page <= Convert.ToInt32(maxPage))
                {
                    string source = WebUtility.HtmlDecode(prog.GetSiteRawHtml(string.Format(url, key.Replace(" ", "%20"), page)));
                    if (!source.Contains("(404) Not Found."))
                    {
                        Console.WriteLine("Crawling [Page {0}]...", page);
                        HtmlDocument html = new HtmlDocument();
                        html.LoadHtml(source);

                        //HtmlNodeCollection nodes = html.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/div[1]/div[5]/table[1]/tr[1]/td[1]/div[6]/table[1]").ChildNodes;
                        IEnumerable<HtmlNode> nodes = html.DocumentNode.Descendants().Where(x => x.Name == "tr"
                                                    && x.Attributes.Contains("class")
                                                    && (x.Attributes["class"].Value.Split().Contains("odd") || x.Attributes["class"].Value.Split().Contains("even")));

                        foreach (HtmlNode node in nodes)
                        {
                            innerhtml = node.InnerHtml;
                            dt.Rows.Add(new object[] { 
                            indexer.ToString(),
                            prog.GetTitle(node.InnerText),
                            Regex.Match(innerhtml, @"((\d{3,4})(p|P)|(\d{3,4}x\d{3,4}))").ToString(),
                            prog.GetNumberOfFiles(innerhtml),
                            prog.GetFileSize(innerhtml),
                            prog.GetMagnetLink(innerhtml),
                            prog.GetTorrentLink(innerhtml),
                            prog.GetTotalSeeders(innerhtml),
                            prog.GetTotalLeechers(innerhtml)
                        });
                            indexer++;
                        }
                    }
                }
                else
                {
                    hasResult = false;
                    Console.WriteLine("Completed!");
                }
            }

            //makes another loop and moved out from the first loop to create more sorting options
            IOrderedEnumerable<DataRow> resulttable = from DataRow row in dt.Rows orderby Convert.ToInt32(row["Seeders"]) descending select row;
            foreach (DataRow row in resulttable)
            {
                using (StreamWriter sw = new StreamWriter("report.txt", true))
                {
                    sw.WriteLine(row["Title"]);
                    sw.WriteLine("Index: {0}\tFiles: {1}\tSeeders: {2}\tLeechers: {3}\tQuality: {4}\tSize: {5}",
                        row["Index"], row["Files"], row["Seeders"], row["Leechers"], row["Resolution"], row["Size"]);
                    sw.WriteLine("Magnet Link: {0}", row["Magnet"]);
                    sw.WriteLine("Torrent Link: {0}", row["Torrent"]);
                    sw.WriteLine();
                    sw.WriteLine();
                }
            }

            if (File.Exists("report.txt"))
            {
                System.Diagnostics.Process.Start("report.txt");
                Console.WriteLine("\nLink Types:\n    *Magnet - Uses the magnet link if available\n    *Torrent - Uses the .torrent file (always available)");

                char ans = 'y';
                while (ans == 'y' || ans == 'Y')
                {
                    Console.Write("\nEnter the INDEX followed by the LINK type to process (e.g. 24 torrent):\n    .>> ");
                    string[] cmds = Console.ReadLine().Split(' ');

                    if (cmds.Count() > 0)
                    {
                        string utorrentPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "uTorrent");
                        if (cmds[1].ToUpper() == "TORRENT")
                        {
                            string file = Path.Combine(utorrentPath, dt.Rows[Convert.ToInt32(cmds[0])]["Title"].ToString().Replace('/', ' ').Replace('\\', ' ') + ".torrent");
                            prog.DownloadTorrent(dt.Rows[Convert.ToInt32(cmds[0])]["Torrent"].ToString(), file);
                            System.Diagnostics.Process.Start(file, Path.Combine(utorrentPath, "utorrent.exe"));
                        }

                        else
                            System.Diagnostics.Process.Start(dt.Rows[Convert.ToInt32(cmds[0])]["Magnet"].ToString(), Path.Combine(utorrentPath, "utorrent.exe"));
                    }

                    Console.Write("\nAdd another INDEX entry? (Y/N):\n    .>> ");
                    ans = Console.ReadLine().ToCharArray()[0];
                }
            }
            else
                Console.WriteLine("No Report Generated!");
            Console.ReadLine();
        }
    }
}
