﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Controls;

namespace WPF_TorrentCrawler_v2
{
    /// <summary>
    /// Interaction logic for CustomContent.xaml
    /// </summary>
    public partial class CustomContent : UserControl
    {
        Crawler crawl = new Crawler();
        private readonly BackgroundWorker bgWorker = new BackgroundWorker();

        private string utorrentPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "uTorrent");
        private string filename;
        private string magnetlink;
        private string torrentlink;


        public CustomContent(DataRow row)
        {
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;

            InitializeComponent();
            lblTitle.Text = row["Title"].ToString();
            lblSize.Content = row["Size"];
            lblSeeders.Content = row["Seeders"];
            lblLeechers.Content = row["Leechers"];
            magnetlink = row["Magnet"].ToString();
            torrentlink = row["Torrent"].ToString();
            filename = row["Title"].ToString().Replace('/', ' ').Replace('\\', ' ').Replace(':', ' ');
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnTorrent.IsEnabled = true;
            btnTorrent.Content = "Download";
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string file = Path.Combine(utorrentPath, filename + ".torrent");
            crawl.DownloadTorrent(torrentlink, file);
            System.Diagnostics.Process.Start(file, Path.Combine(utorrentPath, "utorrent.exe"));
        }

        private void btnMagnet_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(magnetlink, Path.Combine(utorrentPath, "utorrent.exe"));
        }

        private void btnTorrent_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(magnetlink, Path.Combine(utorrentPath, "utorrent.exe"));
        }
    }
}
