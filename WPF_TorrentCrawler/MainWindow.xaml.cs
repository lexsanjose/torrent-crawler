﻿using System;
using System.Data;
using System.Windows;

namespace WPF_TorrentCrawler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Crawler crawl = new Crawler();
        DataTable items;
        int page = 1;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Builder()
        {
            if (items.Rows.Count > 0)
            {
                foreach (DataRow row in items.Rows)
                    SPContainer.Children.Add(new CustomContent(row));
                SPPager.Visibility = Visibility.Visible;
            }
            else
                SPPager.Visibility = Visibility.Hidden;
        }

        private void Loading(bool complete)
        {
            if(complete)
            {
                btnSearch.Content = "SEARCH";
                btnSearch.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = true;
            }
            else
            {
                btnSearch.Content = "Crawling...";
                btnSearch.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = false;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            page = 1;
            SPContainer.Children.Clear();
            items = new DataTable();
            if (!String.IsNullOrWhiteSpace(txtSearch.Text))
            {
                Loading(false);
                items = crawl.Search(txtSearch.Text, 1);
                Loading(true);
                Builder();
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if(page > 1)
            {
                page--;
                lblPage.Content = page.ToString();

                SPContainer.Children.Clear();
                Loading(false);

                items = new DataTable();
                items = crawl.Search(txtSearch.Text, page);
                Loading(true);
                Builder();
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            page++;
            lblPage.Content = page.ToString();

            SPContainer.Children.Clear();
            Loading(false);

            items = new DataTable();
            items = crawl.Search(txtSearch.Text, page);
            Loading(true);
            Builder();
        }
    }
}
