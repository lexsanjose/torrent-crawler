# A Torrent Crawler #
1. Aims to crawl popular torrent sites and consolidate the magnet and torrent links.
2. The motive for the creation of this application is for evading spams, phishing and viruses found in torrent sites.

## TO DOs ##
1. Replace WPF by Electron (http://electron.atom.io/apps/)

### Optional ###
1. Create a dedicated torrent client using MonoTorrent (*Still doesn't see the need for this*)